Asteroids

"Reminds me of my youth" - The Boogie Man
"The art... it speaks to me on a very emotional level. It must be destroyed" - Lee Cudney
"Sometimes it crashes and then one of my eyes won't stop blinking. 10/10!." - John Watson

By John Watson and Lee Cudney


To start a game, start two client programs, and a server. Get the server's ip, and have the clients connect by following the onscreen prompts.

Command Line Arguments:

- Server: C:\PathToExecuteable\SFML_Server.exe PortNumber
- Client: C:\PathToExecuteable\SFML_Client.exe PortNumber ServerAddress


When connected:
- Send a message by typing and pressing enter. All players in your lobby will be able to read your messages.
- type "setName" (without the quotes), type a name, and press enter to change the name that other players see when you send a message.
- type "play" (without the quotes) to be put into a queue to play a game. You will be matched up to another player that is looking for a game, and the game will start.

Game Controls:
	Left arrow - Rotate left
	Right arrow - Rotate Right
	Up arrow - Accelerate!!!

	Space Key - shoot your might laser

Features -
	3 lives each!
	A mighty Laser!
	moderately sucseful time dilation! 
	Asteroids!
	Simulated gravity, including house brewed and Box2D!

