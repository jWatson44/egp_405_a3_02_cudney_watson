#pragma once
#include "GameObject.h"

class GameObjectManager;

const int PADDLE_MOVEMENT_SPEED = 140;

enum MovementDirection
{
	LocalPaddle= -1,
	MOVING_LEFT = 0,
	NOT_MOVING = 1,
	MOVING_RIGHT = 2
};

class Paddle : public GameObject
{
public:
	Paddle(sf::Sprite* sprite, sf::Vector2f pos, float rotation, GameObjectManager* gameObjectManagerRef, bool isLocalPaddle, int playerNum);
	~Paddle() {};

	inline void setMovementDirection(MovementDirection movementDirection) { mCurrentMovementDirection = movementDirection; }
	inline MovementDirection getCurrentMovementDirection(){ return mCurrentMovementDirection; }
	void updatePaddleInformation(float xPos, MovementDirection moveDir);

	inline float getXPos() { return mPos.x; };
	inline void setXPos(float newPos) { mPos.x = newPos; };

	void update(float time) override;
	void draw(sf::RenderWindow* renderWindow);

private:
	MovementDirection mCurrentMovementDirection;
	GameObjectManager* mpGameObjectManager;
	bool mIsLocalPaddle;
	int mPlayerNum;

	int paddleInc;
	float frameStarLoc;
	MovementDirection frameMoveDir;
};