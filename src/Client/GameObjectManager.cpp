#include <iostream>
#include <fstream>
#include <Rand.h>

#include "GameObjectManager.h"
#include "PhysicsManager.h"

#include "Asteroid.h"

#include "Game.h"

GameObjectManager::GameObjectManager(Game* game, GraphicsManager* graphicsManagerRef, PhysicsManager* physicsManager)
	:mpGraphicsManagerRef(graphicsManagerRef)
	,mpPhysicsManagerRef(physicsManager)
	,mpGame(game)
{
	mBulletSpawnAmount = 0;
	mAsteroidSpawnAmount = 0;
	mPlayerNum = 0;
	mUpdateTime = 33.0f / 1000.0f;
	mShouldShutDown = false;
	mpGameObjectArray = std::vector<GameObject*>();
	mpBulletArray = std::vector<Bullet*>();
	liveUI = std::vector<GameObject*>();
}

void GameObjectManager::createLives()
{
	sf::Sprite* sprite = mpGraphicsManagerRef->getAsset(SpriteIndex::LIVE_IMAGE);
	for (int i = 0; i < 3; i++)
	{
		sf::Vector2f pos = sf::Vector2f(15 + (430 * (float)i), 200);
		GameObject* live = new GameObject(sprite, pos, 0);
		liveUI.push_back(live);
	}
}
void GameObjectManager::update()
{
	sortNetworkPackets();

	if (mpLocalShip->isWaitingForDeletion())
	{
		mpLocalShip->resetDeleteMe();
		mpLocalShip->setPosition(sf::Vector2f(mpGraphicsManagerRef->getRenderWindow()->getSize().x / 2, mpGraphicsManagerRef->getRenderWindow()->getSize().y / 2));
		mpGame->playerHasDied = true;

		delete liveUI[liveUI.size() - 1];
		liveUI.erase(liveUI.begin() + (liveUI.size() - 1));

		mpGame->decreasePlayerLives();

		if (mpGame->getNumPlayerLives() <= 0)
		{
			printf("Game Over!");
			mpGame->shutdown();
		}
	}

	mpLocalShip->update(localGameTime);
	mpNetworkShip->update(localGameTime);


	for (unsigned int j = 0; j < mpBulletArray.size(); j++)
	{
		if (mpBulletArray[j]->isWaitingForDeletion())
		{
			// Delete the object
			destroyedBulletID.push_back(mpBulletArray[j]->getBulletID());
			deleteBulletAtIndex(j);
		}
		else
		{
			mpBulletArray[j]->update(localGameTime);
		}
	}

	mAsteroidID.clear();
	mAsteroidID.shrink_to_fit();

	for (int i = 0; i < mpGameObjectArray.size(); i++)
	{
		
		// Checks to see if the asteroid should be split
		if (static_cast<Asteroid*>(mpGameObjectArray[i])->splitSelf)
		{
			mAsteroidID.push_back(static_cast<Asteroid*>(mpGameObjectArray[i])->getAsteroidID());
			splitAsteroidWithID(static_cast<Asteroid*>(mpGameObjectArray[i])->getAsteroidID());
		}
		// Checks to see if the object should be removed
		else if (mpGameObjectArray[i]->isWaitingForDeletion())
		{
			// Delete the object
			//std::cout << static_cast<Asteroid*>(mpGameObjectArray[i])->getAsteroidID();
	
			deleteObjectAtIndex(i);

		}
		else
		{
			mAsteroidID.push_back(static_cast<Asteroid*>(mpGameObjectArray[i])->getAsteroidID());
			mpGameObjectArray[i]->update(mUpdateTime);
		}
	}

	if (mpGameObjectArray.size() == 0)
	{
		mpGame->increaseGameLevel();
		spawnAsteroids(mpGame->getGameLevel(), 6, 20);
	}
}



void GameObjectManager::draw(sf::RenderWindow* renderWindow)
{
	mpGraphicsManagerRef->drawBackground();

	for (unsigned int z = 0; z < liveUI.size(); z++)
	{
		liveUI[z]->draw(renderWindow);
	}

	mpLocalShip->draw(renderWindow);
	mpNetworkShip->draw(renderWindow);

	for (unsigned int j = 0; j < mpBulletArray.size(); j++)
	{
		mpBulletArray[j]->draw(renderWindow);
	}
	//mpNetworkBullet->draw(renderWindow);
	//mpLocalBullet->draw(renderWindow);
	
	for (int unsigned i = 0; i < mpGameObjectArray.size(); i++)
	{
		mpGameObjectArray[i]->draw(renderWindow);
	}
}

void GameObjectManager::createGameObject(SpriteIndex spriteIndex, sf::Vector2f pos, float rotation)
{
	sf::Sprite* sprite = mpGraphicsManagerRef->getAsset(spriteIndex);
	
	Bullet* localBullet;

	// Checks to see if the object is the player, teammate, or ball
	switch (spriteIndex)
	{
	/*case SpriteIndex::MY_PADDLE_ID:
		mpPlayerPaddle = new Paddle(sprite, pos, rotation, this, true, mPlayerNum);
		break;
		*/
	case SpriteIndex::TEAMMATE_SHIP:
		mpNetworkShip= new Ship(mpPhysicsManagerRef, this, sprite, pos, rotation, false, mPlayerNum);
		//mpGameObjectArray.push_back(mpNetworkShip);
		break;
	case SpriteIndex::MY_SHIP:
		mpLocalShip = new Ship(mpPhysicsManagerRef, this, sprite, pos, rotation, true, mPlayerNum);
		//mpGameObjectArray.push_back(mpLocalShip);
		break;
	case SpriteIndex::BULLET_IMAGE:
		localBullet = new Bullet(mpPhysicsManagerRef, sprite, pos, rotation, this, mBulletSpawnAmount);
		localBullet->updateBulletInformation(pos, rotation);
		mBulletSpawnAmount++;
		mpBulletArray.push_back(localBullet);
		break;
	}

}

void GameObjectManager::createPhysicsObject(SpriteIndex spriteIndex, sf::Vector2f pos, float rotation, float density, float friction, b2Vec2 force, AsteroidSize asteroidSize)
{
	sf::Sprite* sprite = mpGraphicsManagerRef->getAsset(spriteIndex);
	Asteroid* asteroid;

	asteroid = new Asteroid(mpPhysicsManagerRef, sprite, pos, rotation, density, friction, force, mAsteroidSpawnAmount, asteroidSize);
	mpGameObjectArray.push_back(asteroid);

	mAsteroidSpawnAmount++;
	std::cout << mAsteroidSpawnAmount << std::endl;
}

void GameObjectManager::upgradeNetwork(sf::Vector2f networkPos, sf::Vector2f networkVel, int networkRot, int timeStamp)
{
	PlayerInfo temp(networkPos, networkVel, networkRot, timeStamp);
	mNetworkArray.push_back(temp);

	sortNetworkPackets();
	
}

void GameObjectManager::upgradeNetwork(sf::Vector2f networkPos, int networkRot, int timeStamp)
{
	PlayerAction temp(networkPos, networkRot, timeStamp);
	mNetworkActionArray.push_back(temp);

	sortNetworkPackets();

}

Asteroid* GameObjectManager::getAsteroidWithID(int id)
{
	for (int i = 0; i < mpGameObjectArray.size(); i++)
	{
		if (static_cast<Asteroid*>(mpGameObjectArray[i])->getAsteroidID() == id)
		{
			return static_cast<Asteroid*>(mpGameObjectArray[i]);
		}
	}

	return nullptr;
}

void GameObjectManager::splitAsteroidWithID(int id)
{
	Asteroid* asteroidToSplit = getAsteroidWithID(id);

	sf::Vector2f spawnPos = sf::Vector2f(asteroidToSplit->getPosition().x, asteroidToSplit->getPosition().y);
	int asteroid1XVel = asteroidToSplit->getVelocity().x;
	int asteroid1YVel = asteroidToSplit->getVelocity().y;
	int asteroid2XVel = -asteroid1XVel;
	int asteroid2YVel = -asteroid1YVel;

	std::string originalId = std::to_string(asteroidToSplit->getAsteroidID());

	switch (asteroidToSplit->getAsteroidSize())
	{
	case LARGE:
	{
		// Remove the current asteroid
		deleteAsteroidWithID(asteroidToSplit->getAsteroidID());

		// Spawn two medium size asteroids
		createPhysicsObject(mpGraphicsManagerRef->getRandomAsteroidSpriteIndex(), spawnPos, 0, .5f, .5f, b2Vec2(asteroid1XVel, asteroid1YVel), MEDIUM);
		createPhysicsObject(mpGraphicsManagerRef->getRandomAsteroidSpriteIndex(), spawnPos, 0, .5f, .5f, b2Vec2(asteroid2XVel, asteroid2YVel), MEDIUM);
		
		break;
	}
	case MEDIUM:
	{

		// Remove the current asteroid
		deleteAsteroidWithID(asteroidToSplit->getAsteroidID());

		// Spawn two small size asteroids
		createPhysicsObject(mpGraphicsManagerRef->getRandomAsteroidSpriteIndex(), spawnPos, 0, .5f, .5f, b2Vec2(asteroid1XVel, asteroid1YVel), SMALL);
		createPhysicsObject(mpGraphicsManagerRef->getRandomAsteroidSpriteIndex(), spawnPos, 0, .5f, .5f, b2Vec2(asteroid2XVel, asteroid2YVel), SMALL);


		break;
	}
	case SMALL:
		// Remove the asteroid
		deleteAsteroidWithID(std::stoi(originalId));
		break;
	default:
		break;
	}
}

void GameObjectManager::spawnAsteroids(int level, int minAsteroidforce, int maxAsteroidForce)
{

	std::string line;
	std::ifstream asteroidDataFile("Assets/AsteroidSpawnData.txt");
	if (asteroidDataFile.is_open())
	{
		// Reads to the current level.
		for (int i = 0; i < level + 1;)
		{
			std::getline(asteroidDataFile, line);
			if (line == "-----")
				i++;
			else if (line == "+++++")
			{
				mpGame->increaseIterationLevel();
				asteroidDataFile.clear();
				asteroidDataFile.seekg(0, std::ios::beg);
				std::getline(asteroidDataFile, line);
			}
		}

		// Reads in the number of asteroids
		std::getline(asteroidDataFile, line);
		int numOfAsteroids = std::stoi(line);

		// Reads in each asteroid and spawns it
		for (int j = 0; j < numOfAsteroids; j++)
		{
			// Reads in asteroid attributes
			std::getline(asteroidDataFile, line);
			int xPos = std::stoi(line);
			std::getline(asteroidDataFile, line);
			int yPos = std::stoi(line);
			std::getline(asteroidDataFile, line);
			int xVel = std::stoi(line);
			std::getline(asteroidDataFile, line);
			int yVel = std::stoi(line);

			// Applies level iteration modifier
			xVel += xVel * mpGame->getIterationLevel() * 5;
			yVel += yVel * mpGame->getIterationLevel() * 5;

			// Spawns asteroid
			createPhysicsObject(mpGraphicsManagerRef->getRandomAsteroidSpriteIndex(), sf::Vector2f(xPos, yPos), 0, .5f, .5f, b2Vec2(xVel, yVel), LARGE);
		}
	}

	asteroidDataFile.close();
}

void GameObjectManager::deleteAllObjects()
{
	/*
	for (unsigned int i = 0; i < mBrickList.size(); i++)
	{
		delete mBrickList[i];
		mBrickList.erase(mBrickList.begin() + i);
	}

	for (unsigned int j = 0; j < mHearts.size(); j++)
	{
		delete mHearts[j];
		mHearts.erase(mHearts.begin() + j);
	}
	mHearts.clear();
	*/

	for (unsigned int j = 0; j < mpBulletArray.size(); j++)
	{
		delete mpBulletArray[j];
		mpBulletArray.erase(mpBulletArray.begin() + j);
	}

	for (unsigned int i = 0; i < mpGameObjectArray.size(); i++)
	{
		delete mpGameObjectArray[i];
		mpGameObjectArray.erase(mpGameObjectArray.begin() + i);
	}

	delete mpNetworkShip;
	delete mpLocalShip;
}

void GameObjectManager::deleteObjectAtIndex(int index)
{
	b2Body* body = static_cast<Asteroid*>(mpGameObjectArray[index])->getBody();
	body->GetWorld()->DestroyBody(body);

	delete mpGameObjectArray[index];
	mpGameObjectArray.erase(mpGameObjectArray.begin() + index);
}

void GameObjectManager::deleteBulletAtIndex(int index)
{
	b2Body* body = mpBulletArray[index]->getBody();
	body->GetWorld()->DestroyBody(body);

	delete mpBulletArray[index];
	mpBulletArray.erase(mpBulletArray.begin() + index);
}

void GameObjectManager::deleteAsteroidWithID(int id)
{
	for (int i = 0; i < mpGameObjectArray.size(); i++)
	{
		if (static_cast<Asteroid*>(mpGameObjectArray[i])->getAsteroidID() == id)
		{
			deleteObjectAtIndex(i);
			break;
		}
	}
}

void GameObjectManager::deleteBulletWithID(int id)
{
	for (int i = 0; i < mpBulletArray.size(); i++)
	{
		if (mpBulletArray[i]->getBulletID() == id)
		{
			destroyedBulletID.push_back(mpBulletArray[i]->getBulletID());
			deleteBulletAtIndex(i);
			break;
		}
	}
}

b2Vec2 GameObjectManager::getRandomAsteroidForce(int rangeStart, int rangeEnd)
{
	b2Vec2 force;
	force.x = rand() % rangeStart + rangeEnd;
	force.y = rand() % rangeStart + rangeEnd;

	bool isNegative = rand() % 2;
	if (isNegative)
		force.x *= -1;

	isNegative = rand() % 2;
	if (isNegative)
		force.y *= -1;

	return force;
}

void GameObjectManager::sortNetworkPackets()
{
	for (int i = 0; i < mNetworkArray.size(); i++)
	{
		if (mNetworkArray[i].playerTimeStamp <= localGameTime + 15)
		{
			mpNetworkShip->updateShipInformation(mNetworkArray[i].playerPos, mNetworkArray[i].playerVel, mNetworkArray[i].playerRot);
			mNetworkArray.erase(mNetworkArray.begin() + i);
			mNetworkArray.shrink_to_fit();
		}
		else
		{
			//std::cout << "too early" << mNetworkActionArray[i].playerTimeStamp << "\n";
		}

	}
	for (int i = 0; i < mNetworkActionArray.size(); i++)
	{
		if (mNetworkActionArray[i].playerTimeStamp <= localGameTime +5)
		{
			//std::cout << mNetworkActionArray[i].playerPos.x << " " << mNetworkActionArray[i].playerPos.y << " - " << mpNetworkShip->getmPosX() << " " << mpNetworkShip->getmPosY() << "\n";
			//mpNetworkBullet->updateBulletInformation((mNetworkActionArray[i].playerPos + mpNetworkShip->getPos() + mpNetworkShip->getPos()) / 3.0f, (mNetworkActionArray[i].playerRot + mpNetworkShip->getRotation())/2.0f);
			createGameObject(BULLET_IMAGE, (mNetworkActionArray[i].playerPos + mpNetworkShip->getPos() + mpNetworkShip->getPos()) / 3.0f, (mNetworkActionArray[i].playerRot + mpNetworkShip->getRotation()) / 2.0f);
			mNetworkActionArray.erase(mNetworkActionArray.begin() + i);
			mNetworkActionArray.shrink_to_fit();
		
		}
		else
		{
			//std::cout << "too early" << mNetworkActionArray[i].playerTimeStamp << "\n";
		}

	}
}