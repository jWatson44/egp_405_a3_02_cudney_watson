#pragma once
#include "PhysicsObject.h"
class GameObjectManager;

class Bullet : public PhysicsObject
{
public:
	Bullet(PhysicsManager* physicsManager, sf::Sprite* sprite, sf::Vector2f pos, float rotation, GameObjectManager* gameObjectManagerRef, int id);
	~Bullet() {};

	void update(float time);
	void draw(sf::RenderWindow* renderWindow);
	void checkEdge();

	void Bullet::updateBulletInformation(sf::Vector2f ShipLoc, float Rot);

	void initializeUserData() override;
	void onCollisionEnter() override;
	void onCollisionExit() override;
	void handleCollisionWith(b2Body* otherObject) override;

	inline void setVelocity(sf::Vector2f newVelocity) { mVelocity = newVelocity; }
	inline void setPosition(sf::Vector2f newPos) { mPos = newPos; }
	inline sf::Vector2f getVelocity(){ return mVelocity; }
	inline sf::Vector2f getPos(){ return mPos; }

	void alignPhysicsBody();

	inline int getBulletID() { return mID; }

private:

	GameObjectManager* mpGameObjectManager;
	bool mIsLocalPaddle;
	int mPlayerNum;
	int rotationCount;

	sf::Vector2f mVelocity;
	sf::Vector2f mForceVector;
	sf::Vector2f mExVel;
	sf::Vector2f mExPos;
	sf::Vector2f mExGoalPos;
	float mExRot;
	float mSpeedCap;
	float mSpeed;
	float mRotateSpeed;
	int mShipInc;
	float updateCount;

	int mCurrentLifeTime;

	bool deleteSelf = false;
	
	int mID;
};