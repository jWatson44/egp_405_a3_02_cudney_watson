#include "PhysicsObject.h"
#include "PhysicsManager.h"

PhysicsObject::PhysicsObject(PhysicsManager* physicsManager, sf::Sprite* sprite, sf::Vector2f pos, float rotation, float density, float friction, PhysicsTag tag)
	:GameObject(sprite, pos, rotation)
	,mpPhysicsManager(physicsManager)
{
	mpPhysicsBody = mpPhysicsManager->createPhysicsObject(mPos.x, mPos.y, density, friction, true);
	mTag = tag;
}

PhysicsObject::PhysicsObject(PhysicsManager* physicsManager, sf::Sprite* sprite, sf::Vector2f pos, float rotation, float density, float friction, b2Vec2 initialVelocity, PhysicsTag tag)
	:GameObject(sprite, pos, rotation)
	,mpPhysicsManager(physicsManager)
{
	mpPhysicsBody = mpPhysicsManager->createPhysicsObject(mPos.x, mPos.y, density, friction, true);
	mpPhysicsBody->SetLinearVelocity(initialVelocity);
	mTag = tag;
}

void PhysicsObject::update(float time)
{
	sf::Vector2f newPos;
	newPos.x = mpPhysicsBody->GetPosition().x;
	newPos.y = mpPhysicsBody->GetPosition().y;

	mPos = newPos;

	GameObject::checkEdge();

	// Sets physics body to the same location as the new position if check edge changed it
	if (mPos != newPos)
		mpPhysicsBody->SetTransform(b2Vec2(mPos.x, mPos.y), mpPhysicsBody->GetAngle());

}

void PhysicsObject::draw(sf::RenderWindow* renderWindow)
{
	mpSprite->setPosition(sf::Vector2f(mpPhysicsBody->GetPosition().x, mpPhysicsBody->GetPosition().y));
	mpSprite->setRotation(mpPhysicsBody->GetAngle());

	renderWindow->draw(*mpSprite);
}

void PhysicsObject::setRotation(float newRotation)
{
	mpPhysicsBody->SetTransform(mpPhysicsBody->GetPosition(), newRotation);
	mRotation = newRotation;
}

void PhysicsObject::setPosition(sf::Vector2f pos)
{
	mPos = pos;

	b2Vec2 physicsPos(pos.x, pos.y);
	mpPhysicsBody->SetTransform(physicsPos, mpPhysicsBody->GetAngle());
}