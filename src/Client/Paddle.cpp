#include "Paddle.h"
#include "GameObjectManager.h"


float PaddleLerp(float startPos, float endPos, float t)
{
	float temp;
	temp = (1 - t) * startPos+ t * endPos;
	return 0.0f;
}
Paddle::Paddle(sf::Sprite* sprite, sf::Vector2f pos, float rotation, GameObjectManager* gameObjectManagerRef, bool isLocalPaddle, int playerNum)
	:GameObject(sprite, pos, rotation)
	,mpGameObjectManager(gameObjectManagerRef)
	,mIsLocalPaddle(isLocalPaddle)
	,mPlayerNum(playerNum)
{
	mCurrentMovementDirection = NOT_MOVING;
	frameMoveDir = LocalPaddle;
}

void Paddle::update(float time)
{
	if (mCurrentMovementDirection != frameMoveDir)
	{
		float paddleMovementSpeed;
		if (mCurrentMovementDirection == MOVING_RIGHT)
			paddleMovementSpeed = PADDLE_MOVEMENT_SPEED;
		else if (mCurrentMovementDirection == MOVING_LEFT)
			paddleMovementSpeed = PADDLE_MOVEMENT_SPEED * -1;
		else
			paddleMovementSpeed = 0;


		mPos.x += paddleMovementSpeed * time;
	}
	else if (mCurrentMovementDirection == frameMoveDir)
	{

		float paddleMovementSpeed;
		if (mCurrentMovementDirection == MOVING_RIGHT)
			paddleMovementSpeed = PADDLE_MOVEMENT_SPEED;
		else if (mCurrentMovementDirection == MOVING_LEFT)
			paddleMovementSpeed = PADDLE_MOVEMENT_SPEED * -1;
		else
			paddleMovementSpeed = 0;

		mPos.x = PaddleLerp(mPos.x, mPos.x += paddleMovementSpeed, paddleInc / 30.0f);
	}
	

}
void Paddle::updatePaddleInformation(float xPos, MovementDirection moveDir)
{
	if (paddleInc >29)
	{
		paddleInc = 0;

	}
	if (paddleInc == 0)
	{
		frameMoveDir = moveDir;
		frameStarLoc = xPos;
		paddleInc++;
	}
	else
	{
		paddleInc++;
		//std::cout << "UPDATE COUNT  " << mBallInc << "\n";
	}
}
void Paddle::draw(sf::RenderWindow* renderWindow)
{

	GameObject::draw(renderWindow);
}