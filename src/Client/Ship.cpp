#include "Ship.h"
#include <iostream>

sf::Vector2f ShipLerp(sf::Vector2f startPos, sf::Vector2f endPos, float a_time)
{
	sf::Vector2f temp;
	temp.x = (1 - a_time) * startPos.x + a_time * endPos.x;
	temp.y = (1 - a_time) * startPos.y + a_time *endPos.y;
	return temp;
}

int Lerp(int startRot, int endRot, float a_time)
{
	int temp;
	temp = (1 - a_time) * startRot + a_time * endRot;
	return temp;

}

Ship::Ship(PhysicsManager* physicsManager, GameObjectManager* gameObjectManager, sf::Sprite* sprite, sf::Vector2f pos, float rotation, bool isLocalPaddle, int playerNum)
	:PhysicsObject(physicsManager, sprite, pos, rotation, 0.0f, 0.0f, b2Vec2(0, 0), PhysicsTag::SHIP)
	, mpGameObjectManager(gameObjectManager)
	, mIsLocalPaddle(isLocalPaddle)
	, mPlayerNum(playerNum)
{
	mVelocity = sf::Vector2f(0, 0);
	mShipInc = 0;
	mSpeedCap = 25.0f;
	mSpeed = 0.6f;
	mRotateSpeed = 1.5f;
	rotationCount = 0;
	updateCount = 10.0f;

	initializeUserData();
}

void Ship::initializeUserData()
{
	mpPhysicsBody->SetUserData((void*)this);
}

void Ship::alignPhysicsBody()
{
	mpPhysicsBody->SetTransform(b2Vec2(mPos.x, mPos.y), mRotation);
}

void Ship::update(float time)
{
	if(mIsLocalPaddle)
	{
		mPos = mPos + mVelocity * 0.15f;
		GameObject::checkEdge();
	}  
	else
	{
		if (mShipInc == updateCount)
		{
			//updateCount++;
		}
		if (abs(mPos.x - mExPos.x) < 1000 && abs(mPos.y - mExPos.y) < 400)
		{
			mPos = ShipLerp(mPos, ((mPos + mPos + mPos+  mExPos) / 4.0f) + (mExVel* 0.14f), (float)mShipInc / updateCount);

			int temp;

			temp = mExRot - mRotation;
			if (temp > 300)
			{

				mRotation = Lerp(mRotation + 360, (mExRot + mRotation + mRotation + mRotation) / 4.0f, (float)mShipInc / updateCount);
				//std::cout << "1" << "\n";
			}
			else if(temp < -200)
			{
				mRotation = Lerp(mRotation - 360, (mExRot + mRotation + mRotation + mRotation) / 4.0f, (float)mShipInc / updateCount);
				//std::cout << "2" << "\n";
				//std::cout << "notfired" << "\n";

			}
			else
			{
				mRotation = Lerp(mRotation, (mExRot + mRotation  + mRotation) / 3.0f, (float)mShipInc / updateCount);
				//std::cout << "3" << "\n";
			}
			//std::cout << mShipInc << " " << updateCount << "\n";
			mShipInc++;
			
		}

		else
		{
			mPos = ShipLerp(mExPos,( mExPos + mExVel)/ 2.0f * 0.15f, (float)mShipInc / updateCount);

			
			
			mShipInc++;
			
			//std::cout << "fired" << mShipInc<< "\n";
		}
		}

		alignPhysicsBody();
		
	}
	
void Ship::accelerateShip()
{
	
	if (std::abs(mVelocity.x + mForceVector.x * mSpeed) < mSpeedCap)
	{
		mVelocity.x += mForceVector.x * mSpeed;
	}
	if (std::abs(mVelocity.y +mForceVector.y * mSpeed) < mSpeedCap)
	{
		mVelocity.y += mForceVector.y * mSpeed;
	}
}
void Ship::rotateShip(rotateDir rotateAmt)
{


	mRotation += rotateAmt * mRotateSpeed;

	if (mRotation > 360)
		mRotation = 0;
	if (mRotation < 0)
		mRotation = 360;

	if (mRotation >= 0 && mRotation < 90)
	{
		mForceVector.x = mRotation / 90;
		mForceVector.y = 1.0f - mForceVector.x ;


		mForceVector.x *= 1.0f;
		mForceVector.y *= -1.0f;
	}
	else if (mRotation >= 90 && mRotation < 180)
	{
		mForceVector.y = (mRotation-90.0f) / 90.0f;
		mForceVector.x = 1.0f - mForceVector.y;

		mForceVector.x *= 1.0f;
		mForceVector.y *= 1.0f;

	}
	else if (mRotation >= 180 && mRotation < 270)
	{
		mForceVector.x = (mRotation - 180.0f) / 90.0f;
		mForceVector.y = 1.0f - mForceVector.x;

		mForceVector.x *= -1.0f;
		mForceVector.y *=1.0f;
	}
	else if (mRotation >= 270 && mRotation < 360 )
	{
		mForceVector.y = (mRotation - 270.0f) / 90.0f;
		mForceVector.x = 1.0f - mForceVector.y;

		mForceVector.x *= -1.0f;
		mForceVector.y *= -1.0f;
	}
	

}
void Ship::updateShipInformation(sf::Vector2f ShipLoc, sf::Vector2f ShipVel, float Rot)
{
	
	mExVel = ShipVel;
	mExPos = ShipLoc;

	mExRot = Rot;
	//std::cout << mShipInc;
	updateCount = mShipInc-1;
	mShipInc = 0;


}
void Ship::draw(sf::RenderWindow* renderWindow)
{
	GameObject::draw(renderWindow);
}

void Ship::onCollisionEnter()
{

}

void Ship::onCollisionExit()
{

}

void Ship::handleCollisionWith(b2Body* otherBody)
{
	switch (static_cast<PhysicsObject*>(otherBody->GetUserData())->getTag())
	{
	case PhysicsTag::ASTEROID:
		deleteThisGameObject();
	default:
		break;
	}
}