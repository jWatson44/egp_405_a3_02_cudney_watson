#pragma once
#include "Box2D\Box2D.h"
#include "ContactListener.h"

class GameObject;

const float32 TIME_STEP = 1.0f / 60.0f;

const int32 VELOCITY_ITERATIONS = 6;
const int32 POSITION_ITERATIONS = 2;

class PhysicsManager
{
public:
	
	PhysicsManager();
	~PhysicsManager();
	
	void initPhysicsWorld(b2Vec2 gravity);
	void uninitPhysicsWorld();
	b2Body* createPhysicsObject(int xPos, int yPos, float density, float friction, bool isSensor);
	inline b2World* getWorld(){ return mpWorld; }


	void updateWorld();

private:

	b2Vec2 mGravity;
	b2World* mpWorld;
	b2ContactListener* mpContactListener;

	const float mKUpdateInterval = 1.0f / 60.0f;
	const double mKSecondsPerUpdate = 0.1f;

	double getCurrentTimeInSeconds();
};