#pragma once
#include "GameObject.h"

class Brick : public GameObject
{
public:
	Brick(sf::Sprite* sprite, sf::Vector2f pos, float rotation);
	~Brick() {};

	void update(float time);
	void draw(sf::RenderWindow* renderWindow);

};