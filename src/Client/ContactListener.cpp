#include "ContactListener.h"
#include "Asteroid.h"
//#include "bullet.h"

ContactListener::ContactListener()
{
	mIsContacting = false;
}

ContactListener::~ContactListener()
{
}

void ContactListener::BeginContact(b2Contact* contact)
{
	if (contact != nullptr)
	{
		// Allows physics objects to handle their own reactions to the collision
		b2Body* body1 = contact->GetFixtureA()->GetBody();
		b2Body* body2 = contact->GetFixtureB()->GetBody();
		if (body1 && body2)
		{
			static_cast<PhysicsObject*>(body1->GetUserData())->handleCollisionWith(body2);
			static_cast<PhysicsObject*>(body2->GetUserData())->handleCollisionWith(body1);
		}
	}
}

void ContactListener::EndContact(b2Contact* contact)
{
	// Check if fixture a was a ball 
	void* bodyUserData = contact->GetFixtureA()->GetBody()->GetUserData();
	if (bodyUserData)
		static_cast<PhysicsObject*>(bodyUserData)->onCollisionExit();

	void* bodyUserData2 = contact->GetFixtureB()->GetBody()->GetUserData();
	if (bodyUserData2)
		static_cast<PhysicsObject*>(bodyUserData2)->onCollisionExit();
}

