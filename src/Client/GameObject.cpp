#include "GameObject.h"


GameObject::GameObject(sf::Sprite* sprite, sf::Vector2f pos, float rotation) 
	:mpSprite(sprite)
	,mPos(pos)
	,mRotation(rotation)
{
}

void GameObject::update(float time)
{

}

void GameObject::draw(sf::RenderWindow* renderWindow)
{
	mpSprite->setPosition(mPos);
	mpSprite->setRotation(mRotation);

	renderWindow->draw(*mpSprite);
}

void GameObject::checkEdge()
{
	if (mPos.x < 0)
		mPos.x = 1240;
	if (mPos.x > 1240)
		mPos.x = 0;
	if (mPos.y < 0)
		mPos.y = 690;
	if (mPos.y > 690)
		mPos.y = 0;	
}

void GameObject::setPosition(sf::Vector2f pos)
{
	mPos = pos;
}