#include "InputManager.h"


InputManager::InputManager()
{
	mEscapePressed = true;
	mLeftKeyPressed = false;
	mRightKeyPressed = false;
	mPreviousLeftKeyPressed = false;
	mPreviousRightKeyPressed = false;
}

InputManager::~InputManager()
{
}

void InputManager::update()
{
	mPreviousLeftKeyPressed = mLeftKeyPressed;
	mPreviousRightKeyPressed = mRightKeyPressed;
	mPreviousSpaceKeyPressed = mSpaceKeyPressed;

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
	{
		mLeftKeyPressed = true;
	}
	if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
	{
		mLeftKeyPressed = false;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
	{
		mRightKeyPressed = true;
	}
	if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
	{
		mRightKeyPressed = false;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
	{
		mUpKeyPressed = true;
	}
	if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
	{
		mUpKeyPressed = false;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
	{
		mSpaceKeyPressed = true;
	}
	if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
	{
		mSpaceKeyPressed = false;
	}
	// Shutdown game
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
	{
		mEscapePressed = true;
	}
	else
		mEscapePressed = false;
}

bool InputManager::didRightKeyChange()
{
	if (mRightKeyPressed != mPreviousRightKeyPressed)
		return true;
	else
		return false;
}

bool InputManager::didLeftKeyChange()
{
	if (mLeftKeyPressed != mPreviousLeftKeyPressed)
		return true;
	else
		return false;
}
bool InputManager::didSpaceKeyChange()
{
	if (mSpaceKeyPressed != mPreviousSpaceKeyPressed)
		return true;
	else
		return false;
}

