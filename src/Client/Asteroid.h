#pragma once
#include "SFML\Graphics.hpp"
#include "Box2D\Box2D.h"
#include "PhysicsObject.h"

class PhysicsManager;

enum AsteroidSize
{
	LARGE = 2,
	MEDIUM = 1,
	SMALL = 0
};

class Asteroid : public PhysicsObject
{
public:
	Asteroid(PhysicsManager* physicsManager, sf::Sprite* sprite, sf::Vector2f pos, float rotation, float density, float friction, int asteroidID, AsteroidSize asteroidSize);
	Asteroid(PhysicsManager* physicsManager, sf::Sprite* sprite, sf::Vector2f pos, float rotation, float density, float friction, b2Vec2 initialVelocity, int asteroidID, AsteroidSize asteroidSize);
	~Asteroid();

	void initializeUserData() override;
	void onCollisionEnter() override;
	void onCollisionExit() override;
	void handleCollisionWith(b2Body* otherObject) override;

	bool deleteSelf = false;
	bool splitSelf = false;

	inline int getAsteroidID() { return mID; }
	inline AsteroidSize getAsteroidSize() { return mCurrentAsteroidSize; }

private:
	int mID;
	AsteroidSize mCurrentAsteroidSize;
};