#pragma once
#include "GameObject.h"


class RewardBrick : public GameObject
{
public:
	RewardBrick(sf::Sprite* sprite, sf::Vector2f pos, float rotation);
	~RewardBrick();

	void update(float time);
	void draw(sf::RenderWindow* renderWindow);


private:
	sf::Vector2f mPos;
	float mRotation;
	sf::Sprite* mpSprite;

};