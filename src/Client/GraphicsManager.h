#pragma once
#include "SFML/Graphics.hpp"


enum SpriteIndex
{
	MY_SHIP = 1,
	TEAMMATE_SHIP,
	ASTEROID_TYPE_1,
	ASTEROID_TYPE_2,
	ASTEROID_TYPE_3,
	STAR_FEILD_BACKGROUND,
	BULLET_IMAGE,
	LIVE_IMAGE
};

class GraphicsManager
{
public:

	GraphicsManager();
	~GraphicsManager() {};

	void uninit();

	void startGraphics(int screenWidth, int screenHeight);
	void startGraphics(sf::Vector2i screenDimensions);
	void draw();
	void shutdownGraphics();

	sf::Sprite* getAsset(SpriteIndex spriteIndex);

	bool inline haveGraphicsStarted() { return mGraphicsStarted; };
	sf::RenderWindow* getRenderWindow() { return mWindow; }

	inline void clearWindow(){ mWindow->clear(); }

	void drawBackground();

	SpriteIndex getRandomAsteroidSpriteIndex();


private:
	// Window
	sf::RenderWindow* mWindow;

	// Textures
	sf::Texture mBackgroundTexture;
	sf::Texture mPlayerShipTexture;
	sf::Texture mTeammateShipTexture;
	sf::Texture mAsteroidTexture1;
	sf::Texture mAsteroidTexture2;
	sf::Texture mAsteroidTexture3;
	sf::Texture mBulletTexture;
	sf::Texture mLiveTexture;

	// Sprites
	sf::Sprite* mpBackgroundSprite;
	sf::Sprite* mpPlayerShipSprite;
	sf::Sprite* mpTeammateShipSprite;
	sf::Sprite* mpAsteroidSprite1;
	sf::Sprite* mpAsteroidSprite2;
	sf::Sprite* mpAsteroidSprite3;
	sf::Sprite* mpBulletSprite;
	sf::Sprite* mpLiveSprite;


	// Screen Dimensions
	sf::Vector2i mScreenDimensions;

	bool mGraphicsStarted;
};