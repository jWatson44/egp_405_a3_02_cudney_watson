#include "Asteroid.h"
#include "PhysicsManager.h"

Asteroid::Asteroid(PhysicsManager* physicsManager, sf::Sprite* sprite, sf::Vector2f pos, float rotation, float density, float friction, int asteroidID, AsteroidSize asteroidSize)
	:PhysicsObject(physicsManager, sprite, pos, rotation, density, friction, PhysicsTag::ASTEROID)
	, mID(asteroidID)
	, mCurrentAsteroidSize(asteroidSize)
{
	splitSelf = false;
	deleteSelf = false;

	switch (mCurrentAsteroidSize)
	{
	case LARGE:
		sprite->setScale(sf::Vector2f(5, 5));
		mpPhysicsBody->GetFixtureList()->GetShape()->m_radius = 10;
		break;
	case MEDIUM:
		sprite->setScale(sf::Vector2f(2.5f, 2.5f));
		mpPhysicsBody->GetFixtureList()->GetShape()->m_radius = 10;
		break;
	default:
		sprite->setScale(sf::Vector2f(1, 1));
		mpPhysicsBody->GetFixtureList()->GetShape()->m_radius = 10;
		break;
	}

	initializeUserData();
}

Asteroid::Asteroid(PhysicsManager* physicsManager, sf::Sprite* sprite, sf::Vector2f pos, float rotation, float density, float friction, b2Vec2 initialVelocity, int asteroidID, AsteroidSize asteroidSize)
	: PhysicsObject(physicsManager, sprite, pos, rotation, density, friction, initialVelocity, PhysicsTag::ASTEROID)
	, mID(asteroidID)
	, mCurrentAsteroidSize(asteroidSize)
{
	splitSelf = false;
	deleteSelf = false;

	initializeUserData();
}

void Asteroid::initializeUserData()
{
	mpPhysicsBody->SetUserData((void*)this);
}

void Asteroid::onCollisionEnter()
{

}

void Asteroid::onCollisionExit()
{

}

void Asteroid::handleCollisionWith(b2Body* otherBody)
{
	switch (static_cast<PhysicsObject*>(otherBody->GetUserData())->getTag())
	{
	case PhysicsTag::BULLET:
		deleteThisGameObject();//splitSelf = true;
		break;
	default:
		break;
	}
}


