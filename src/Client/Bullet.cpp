#include "Bullet.h"
#include <iostream>



Bullet::Bullet(PhysicsManager* physicsManager, sf::Sprite* sprite, sf::Vector2f pos, float rotation, GameObjectManager* gameObjectManagerRef, int id)
	:PhysicsObject(physicsManager, sprite, pos, rotation, 0.0f, 0.0f, b2Vec2(0, 0), PhysicsTag::BULLET)
	,mpGameObjectManager(gameObjectManagerRef)
	, mID(id)
{
	mVelocity = sf::Vector2f(0, 0);
	mShipInc = 0;
	mSpeedCap = 35.0f;
	mSpeed = 0.8f;
	mRotateSpeed = 1.0f;
	rotationCount = 0;
	updateCount = 10.0f;

	mCurrentLifeTime = 50;

	initializeUserData();
}

void Bullet::update(float time)
{
	mPos = mPos + mVelocity *20.0f;
	GameObject::checkEdge();

	alignPhysicsBody();

	mCurrentLifeTime--;
	if (mCurrentLifeTime <= 0)
		mDeleteMe = true;
}

void Bullet::initializeUserData()
{
	mpPhysicsBody->SetUserData((void*)this);
}

void Bullet::alignPhysicsBody()
{
	mpPhysicsBody->SetTransform(b2Vec2(mPos.x, mPos.y), mRotation);
}
	
void Bullet::updateBulletInformation(sf::Vector2f ShipLoc, float Rot)
{
	mRotation = (int)Rot;

	mPos = ShipLoc;
	if (mRotation >= 0 && mRotation < 90)
	{
		mForceVector.x = mRotation / 90;
		mForceVector.y = 1.0f - mForceVector.x;


		mForceVector.x *= 1.0f;
		mForceVector.y *= -1.0f;
	}
	else if (mRotation >= 90 && mRotation < 180)
	{
		mForceVector.y = (mRotation - 90.0f) / 90.0f;
		mForceVector.x = 1.0f - mForceVector.y;

		mForceVector.x *= 1.0f;
		mForceVector.y *= 1.0f;

	}
	else if (mRotation >= 180 && mRotation < 270)
	{
		mForceVector.x = (mRotation - 180.0f) / 90.0f;
		mForceVector.y = 1.0f - mForceVector.x;

		mForceVector.x *= -1.0f;
		mForceVector.y *= 1.0f;
	}
	else if (mRotation >= 270 && mRotation < 360)
	{
		mForceVector.y = (mRotation - 270.0f) / 90.0f;
		mForceVector.x = 1.0f - mForceVector.y;

		mForceVector.x *= -1.0f;
		mForceVector.y *= -1.0f;
	}
	mVelocity = mSpeed * mForceVector;


}
void Bullet::draw(sf::RenderWindow* renderWindow)
{
	GameObject::draw(renderWindow);
}

void Bullet::onCollisionEnter()
{

}

void Bullet::onCollisionExit()
{

}

void Bullet::handleCollisionWith(b2Body* otherBody)
{
	switch (static_cast<PhysicsObject*>(otherBody->GetUserData())->getTag())
	{
	case PhysicsTag::ASTEROID:
		printf("\nBullet Hit Asteroid");
		deleteThisGameObject();
	default:
		break;
	}
}