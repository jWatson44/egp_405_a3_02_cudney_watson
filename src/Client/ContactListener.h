#pragma once
#include "Box2D\Box2D.h"

class ContactListener : public b2ContactListener
{
public:
	
	ContactListener();
	~ContactListener();

	void BeginContact(b2Contact* contact) override;
	void handleContactResults();
	void EndContact(b2Contact* contact) override;

	b2Fixture* GetFixtureA();
	b2Fixture* GetFixtureB();

private:
	bool mIsContacting;

};