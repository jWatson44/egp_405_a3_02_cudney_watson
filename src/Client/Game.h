#pragma once
#include "GraphicsManager.h"
#include "GameObjectManager.h"
#include "InputManager.h"
#include "PhysicsManager.h"

const sf::Vector2i SCREEN_DIMENSIONS = sf::Vector2i(1280, 720);

class Game
{
public:
	Game();
	~Game(){};

	void init();
	void startup(int playerNum);
	void shutdown();
	void uninit();

	void update();

	inline Game* getGame() { return this; }
	inline GraphicsManager* getGraphicsManager() { return mpGraphicsManager; }
	inline InputManager* getInputManager() { return mpInputManager; }
	inline GameObjectManager* getGameObjectManager() { return mpGameObjectManager; }
	inline PhysicsManager* getPhysicsManager(){ return mpPhysicsManager; }
	
	inline int getPlayerNum() { return mPlayerNum; }
	inline void setPlayerNumber(int playerNumber) { mPlayerNum = playerNumber; }
	inline bool isGameStarted() { return bGameStarted; }

	inline void increaseGameLevel() { mGameLevel++; }
	inline int getGameLevel() { return mGameLevel; }

	inline void increaseIterationLevel() { mGameLevelIteration++; }
	inline int getIterationLevel() { mGameLevel = 0; return mGameLevelIteration; }

	inline void decreasePlayerLives() { playerLives--; }
	inline int getNumPlayerLives() { return playerLives; }
	bool playerHasDied;

private:
	GraphicsManager* mpGraphicsManager;
	InputManager* mpInputManager;
	GameObjectManager* mpGameObjectManager;
	PhysicsManager* mpPhysicsManager;

	bool bGameStarted;
	int mPlayerNum;

	float mGameLevel;
	float mGameLevelIteration;

	int playerLives;

};