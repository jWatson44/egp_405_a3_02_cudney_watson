#include "Brick.h"

Brick::Brick(sf::Sprite* sprite, sf::Vector2f pos, float rotation)
	:GameObject(sprite, pos, rotation) {}


void Brick::update(float time)
{
	// Check if hit?
}

void Brick::draw(sf::RenderWindow* renderWindow)
{
	GameObject::draw(renderWindow);
}