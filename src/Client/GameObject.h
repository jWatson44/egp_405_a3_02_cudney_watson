#pragma once
#include "SFML\Graphics.hpp"

class GameObject
{
public:
	GameObject(sf::Sprite* sprite, sf::Vector2f pos, float rotation);
	~GameObject() {};

	virtual void update(float time);
	virtual void draw(sf::RenderWindow* renderWindow);

	inline float getmPosX(){ return mPos.x; }
	inline float getmPosY(){ return mPos.y; }
	inline float getRotation(){ return mRotation; }

	virtual inline void setRotation(float newRotation){ mRotation = newRotation; }

	virtual void setPosition(sf::Vector2f newPos);

	inline virtual void deleteThisGameObject() { mDeleteMe = true; }
	inline bool isWaitingForDeletion() { return mDeleteMe; }
	inline void resetDeleteMe() { mDeleteMe = false; }

protected:
	sf::Vector2f mPos;
	float mRotation;
	sf::Sprite* mpSprite;

	void checkEdge();

	bool mDeleteMe = false; // Flags whether the game object manager should remove the gameobject the next time it is updated.
};