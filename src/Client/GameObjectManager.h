#pragma once
#include <vector>
#include <stdio.h>
#include <iostream>
#include <string.h>
#include "GraphicsManager.h"

#include "GameObject.h"
#include "Paddle.h"
#include "Ship.h"
#include "Brick.h"
#include "RewardBrick.h"
#include "Bullet.h"

#include "Box2D\Box2D.h"

#include "Asteroid.h"

class PhysicsManager;
class Game;


struct PlayerInfo
{
	sf::Vector2f playerPos;
	sf::Vector2f playerVel;
	int playerRot;
	int playerTimeStamp;
	PlayerInfo(sf::Vector2f Pos, sf::Vector2f Vel, int Rot, int timeStamp)
		:playerPos(Pos), playerVel(Vel), playerRot(Rot), playerTimeStamp(timeStamp)
	{}
	PlayerInfo()
	{}

};
struct PlayerAction
{
	sf::Vector2f playerPos;
	int playerRot;
	int playerTimeStamp;
	PlayerAction(sf::Vector2f Pos, int Rot, int timeStamp)
		:playerPos(Pos), playerRot(Rot), playerTimeStamp(timeStamp)
	{}
	PlayerAction()
	{}

};
class GameObjectManager
{
public:
	GameObjectManager(Game* game, GraphicsManager* graphicsManagerRef, PhysicsManager* physicsManager);
	~GameObjectManager(){};

	void update();
	void draw(sf::RenderWindow* renderWindow);

	void deleteAllObjects();
	void deleteObjectAtIndex(int index);
	void deleteAsteroidWithID(int id);
	void deleteBulletWithID(int id);
	void deleteBulletAtIndex(int index);

	void sortNetworkPackets();


	void createGameObject(SpriteIndex spriteIndex, sf::Vector2f pos, float rotation);
	void createPhysicsObject(SpriteIndex spriteIndex, sf::Vector2f pos, float rotation, float density, float friction, b2Vec2 force, AsteroidSize asteroidSize);
	void createLives();

	void spawnAsteroids(int level, int minAsteroidForce, int maxAsteroidForce);

	Asteroid* getAsteroidWithID(int id);
	void splitAsteroidWithID(int id);

	void upgradeNetwork(sf::Vector2f networkPos, sf::Vector2f networkVel, int networkRot, int timeStamp);
	void upgradeNetwork(sf::Vector2f networkPos, int networkRot, int timeStamp);
	inline void setPlayerNum(int playerNum) { mPlayerNum = playerNum; }
	inline bool shouldShutdown() { return mShouldShutDown; }
	inline Ship* getLocalShip() {return mpLocalShip;}
	//inline Bullet* getLocalBullet() { return  mpLocalBullet; }
	inline void setGameTime(int newTIme) { localGameTime = newTIme; };


	inline std::vector<int> getDestroyedBullets() { return destroyedBulletID; }

	inline std::vector<int> getAsteroids() { return mAsteroidID; }

private:
	Game* mpGame;

	GraphicsManager* mpGraphicsManagerRef;
	PhysicsManager* mpPhysicsManagerRef;

	std::vector<GameObject*> mpGameObjectArray;
	Ship* mpLocalShip;
	Ship* mpNetworkShip;
	
	//Bullet* mpLocalBullet;
	//Bullet* mpNetworkBullet;
	int localGameTime;
	std::vector<PlayerInfo> mNetworkArray;
	std::vector<PlayerAction> mNetworkActionArray;
	float mUpdateTime;
	int mPlayerNum;
	
	std::vector<int> destroyedAsteroidID;
	std::vector<int> destroyedBulletID;

	std::vector<GameObject*> liveUI;

	std::vector<int> mAsteroidID;
	bool mShouldShutDown;

	std::vector<Bullet*> mpBulletArray;

	int mBulletSpawnAmount;
	int mAsteroidSpawnAmount;

	b2Vec2 getRandomAsteroidForce(int rangeStart, int rangeEnd);
};