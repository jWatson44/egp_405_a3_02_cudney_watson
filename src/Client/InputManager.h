#include "SFML\Window\Keyboard.hpp"

class InputManager
{
public:
	InputManager();
	~InputManager();

	void update();
	
	inline bool isLeftKeyDown() { return mLeftKeyPressed; }
	inline bool isRightKeyDown() { return mRightKeyPressed; }
	inline bool isUpKeyDown() { return mUpKeyPressed; }
	inline bool isSpaceKeyDown() { return mSpaceKeyPressed; }

	bool didLeftKeyChange();
	bool didRightKeyChange();
	bool didSpaceKeyChange();
	bool escapePressed() { return mEscapePressed; }

private:
	bool mLeftKeyPressed;
	bool mPreviousLeftKeyPressed;

	bool mRightKeyPressed;
	bool mPreviousRightKeyPressed;

	bool mPreviousSpaceKeyPressed;

	bool mUpKeyPressed;

	bool mSpaceKeyPressed;
	bool mEscapePressed;
};