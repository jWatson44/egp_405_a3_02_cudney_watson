#include "GraphicsManager.h"

GraphicsManager::GraphicsManager()
{
	mGraphicsStarted = false;

	// Sprites
	mpBackgroundSprite = nullptr;
	mpPlayerShipSprite = nullptr;
	mpTeammateShipSprite = nullptr;
	mpAsteroidSprite1 = nullptr;
	mpAsteroidSprite2 = nullptr;
	mpAsteroidSprite3 = nullptr;
	mpBulletSprite = nullptr;
	mpLiveSprite = nullptr;


}

void GraphicsManager::uninit()
{

	delete mpBackgroundSprite;
	delete mpPlayerShipSprite;
	delete mpTeammateShipSprite;
	delete mpAsteroidSprite1;
	delete mpAsteroidSprite2;
	delete mpAsteroidSprite3;
	delete mpBulletSprite;
	delete mpLiveSprite;

	delete mWindow;
}

void GraphicsManager::startGraphics(int screenWidth, int screenHeight)
{
	mScreenDimensions = sf::Vector2i(screenWidth, screenHeight);
	mWindow = new sf::RenderWindow(sf::VideoMode(screenWidth, screenHeight), "hit the ball into the bricks with the paddle game");
	mWindow->setFramerateLimit(60);

	mWindow->clear(sf::Color::Black);

	mGraphicsStarted = true;
}

void GraphicsManager::startGraphics(sf::Vector2i screenDimensions)
{
	mScreenDimensions = screenDimensions;
	mWindow = new sf::RenderWindow(sf::VideoMode(screenDimensions.x, screenDimensions.y), "hit the ball into the bricks with the paddle game");

	mpBackgroundSprite = getAsset(STAR_FEILD_BACKGROUND);

	mWindow->setFramerateLimit(60);

	mWindow->clear(sf::Color::Black);
	drawBackground();

	mGraphicsStarted = true;

}

void GraphicsManager::draw()
{
	mWindow->display();
}

void GraphicsManager::drawBackground()
{
	mWindow->draw(*mpBackgroundSprite);
}

void GraphicsManager::shutdownGraphics()
{
	mWindow->close();
}

SpriteIndex GraphicsManager::getRandomAsteroidSpriteIndex()
{
	// Choses random asteroid sprite
	int randomAsteroidNum = rand() % 3;

	switch (randomAsteroidNum)
	{
	case 0:
		return ASTEROID_TYPE_1;
	case 1:
		return ASTEROID_TYPE_2;
	case 2:
		return ASTEROID_TYPE_3;
	default:
		break;
	}
}

sf::Sprite* GraphicsManager::getAsset(SpriteIndex spriteIndex)
{
	switch (spriteIndex)
	{
	case MY_SHIP:
		if (mpPlayerShipSprite == nullptr)
		{
			if (!mPlayerShipTexture.loadFromFile("Assets/Player1Ship.png"))
			{
				printf("Error loading My Ship.");
				return false;
			}
			mpPlayerShipSprite = new sf::Sprite(mPlayerShipTexture);
			mpPlayerShipSprite->setOrigin(mpPlayerShipSprite->getTextureRect().width / 2, mpPlayerShipSprite->getTextureRect().height / 2);

		}

		return mpPlayerShipSprite;
		break;

	case TEAMMATE_SHIP:
		if (mpTeammateShipSprite == nullptr)
		{
			if (!mTeammateShipTexture.loadFromFile("Assets/Player2Ship.png"))
			{
				printf("Error loading Teammate Ship.");
				return false;
			}
			mpTeammateShipSprite = new sf::Sprite(mTeammateShipTexture);
			mpTeammateShipSprite->setOrigin(mpTeammateShipSprite->getTextureRect().width / 2, mpTeammateShipSprite->getTextureRect().height / 2);

		}

		return mpTeammateShipSprite;
		break;

	case LIVE_IMAGE:
		if (mpLiveSprite == nullptr)
		{
			if (!mLiveTexture.loadFromFile("Assets/Planet.png"))
			{
				printf("Error loading planet.");
				return false;
			}
			mpLiveSprite = new sf::Sprite(mLiveTexture);

		}

		return mpLiveSprite;
		break;


	case ASTEROID_TYPE_1:
		if (mpAsteroidSprite1 == nullptr)
		{
			if (!mAsteroidTexture1.loadFromFile("Assets/Asteroid1.png"))
			{
				printf("Error loading Asteroid 1.");
				return false;
			}
			mpAsteroidSprite1 = new sf::Sprite(mAsteroidTexture1);
		}

		return mpAsteroidSprite1;
		break;

	case ASTEROID_TYPE_2:
		if (mpAsteroidSprite2 == nullptr)
		{
			if (!mAsteroidTexture2.loadFromFile("Assets/Asteroid2.png"))
			{
				printf("Error loading Asteroid 2.");
				return false;
			}
			mpAsteroidSprite2 = new sf::Sprite(mAsteroidTexture2);
		}

		return mpAsteroidSprite2;
		break;

	case ASTEROID_TYPE_3:
		if (mpAsteroidSprite3 == nullptr)
		{
			if (!mAsteroidTexture3.loadFromFile("Assets/Asteroid3.png"))
			{
				printf("Error loading Asteroid 3.");
				return false;
			}
			mpAsteroidSprite3 = new sf::Sprite(mAsteroidTexture3);
		}

		return mpAsteroidSprite3;
		break;

	case BULLET_IMAGE:
		if (mpBulletSprite == nullptr)
		{
			if (!mBulletTexture.loadFromFile("Assets/Bullet.png"))
			{
				printf("Error loading Bullet.");
				return false;
			}
			mpBulletSprite = new sf::Sprite(mBulletTexture);
		}

		return mpBulletSprite;
		break;

	case STAR_FEILD_BACKGROUND:
		if (mpBackgroundSprite == nullptr)
		{
			if (!mBackgroundTexture.loadFromFile("Assets/StarField.png"))
			{
				printf("Error loading Background.");
				return false;
			}
			mpBackgroundSprite = new sf::Sprite(mBackgroundTexture);
		}

		return mpBackgroundSprite;
		break;

	default:
		return nullptr;
	}
}

