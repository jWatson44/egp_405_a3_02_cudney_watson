#pragma once
#include "PhysicsObject.h"
class GameObjectManager;
const float Ship_MOVEMENT_SPEED = 3;

enum rotateDir
{
	_LEFT = -1,
	_NONE = 0,
	_RIGHT = 1
};

class Ship : public PhysicsObject
{
public:
	Ship(PhysicsManager* physicsManager, GameObjectManager* gameObjectManager, sf::Sprite* sprite, sf::Vector2f pos, float rotation, bool isLocalPaddle, int playerNum);
	~Ship() {};

	void update(float time);
	void draw(sf::RenderWindow* renderWindow);
	void checkEdge();

	void alignPhysicsBody();

	void Ship::updateShipInformation(sf::Vector2f ShipLoc, sf::Vector2f ShipVel, float Rot);

	void accelerateShip();
	void rotateShip(rotateDir rotateAmt);

	void initializeUserData() override;
	void onCollisionEnter() override;
	void onCollisionExit() override;
	void handleCollisionWith(b2Body* otherObject) override;

	inline void setVelocity(sf::Vector2f newVelocity) { mVelocity = newVelocity; }
	inline void setPosition(sf::Vector2f newPos) { mPos = newPos; }
	inline sf::Vector2f getVelocity(){ return mVelocity; }
	inline sf::Vector2f getPos(){ return mPos; }


private:

	GameObjectManager* mpGameObjectManager;
	bool mIsLocalPaddle;
	int mPlayerNum;
	int rotationCount;

	sf::Vector2f mVelocity;
	sf::Vector2f mForceVector;
	sf::Vector2f mExVel;
	sf::Vector2f mExPos;
	sf::Vector2f mExGoalPos;
	float mExRot;
	float mSpeedCap;
	float mSpeed;
	float mRotateSpeed;
	int mShipInc;
	float updateCount;
};