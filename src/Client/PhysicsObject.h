#pragma once
#include "SFML\Graphics.hpp"
#include "Box2D\Box2D.h"

#include "GameObject.h"
#include "ContactListener.h"

class PhysicsManager;

enum PhysicsTag
{
	NONE = 0,
	ASTEROID = 1,
	BULLET = 2,
	SHIP = 3
};

class PhysicsObject : public GameObject
{
public:
	PhysicsObject(PhysicsManager* physicsManager, sf::Sprite* sprite, sf::Vector2f pos, float rotation, float density, float friction, PhysicsTag tag);
	PhysicsObject(PhysicsManager* physicsManager, sf::Sprite* sprite, sf::Vector2f pos, float rotation, float density, float friction, b2Vec2 initialVelocity, PhysicsTag tag);
	~PhysicsObject() {};

	virtual void update(float time);
	virtual void draw(sf::RenderWindow* renderWindow) override;

	virtual void initializeUserData() = 0;
	virtual void onCollisionEnter() = 0;
	virtual void handleCollisionWith(b2Body* otherBody) = 0;
	virtual void onCollisionExit() = 0;

	inline void setTag(PhysicsTag newTag) { mTag = newTag; }
	inline PhysicsTag getTag() { return mTag; }

	void setRotation(float newRotation) override;
	void setPosition(sf::Vector2f pos) override;

	inline float getRotation() { return mpPhysicsBody->GetAngle(); }
	inline b2Vec2 getPosition() { return mpPhysicsBody->GetPosition(); }

	inline void setVelocity(sf::Vector2f newVelocity) { mpPhysicsBody->SetLinearVelocity(b2Vec2(newVelocity.x, newVelocity.y)); }
	inline b2Vec2 getVelocity(){ return mpPhysicsBody->GetLinearVelocity(); }

	inline b2Body* getBody() { return mpPhysicsBody; }

protected:
	PhysicsManager* mpPhysicsManager;
	b2Body* mpPhysicsBody;
	PhysicsTag mTag;

	
};