#include <stdio.h>
#include <iostream>
#include <string.h>
#include "RakPeerInterface.h"
#include "Kbhit.h"
#include "MessageIdentifiers.h"
#include "BitStream.h"
#include "RakNetTypes.h"  // MessageID
#include "Gets.h"
#include "RakNetTime.h"
#include "Timer.h"
#include "Game.h"
#include "GetTime.h"
#define MAX_CLIENTS 10
#define SERVER_PORT 60000

const float UPDATE_TIME = 16.67f;

struct GameState
{
	float challengerPos = 360.0f;
	float localPlayerPos = 500.0f;
};
struct ClientState
{
	long unsigned int m_id;
	int x, y;
};
struct LocalPlayerInfo
{
	sf::Vector2f playerPos;
	sf::Vector2f playerVel;
	int playerRot; 
	RakNet::Time playerTimeStamp;
	LocalPlayerInfo(sf::Vector2f Pos, sf::Vector2f Vel, int Rot, RakNet::Time timeStamp)
		:playerPos(Pos), playerVel(Vel), playerRot(Rot), playerTimeStamp(timeStamp)
	{}
	LocalPlayerInfo()
	{}

};

struct LocalPlayerAction
{
	sf::Vector2f playerPos;
	int playerRot;
	RakNet::Time playerTimeStamp;
	LocalPlayerAction(sf::Vector2f Pos, int Rot, RakNet::Time timeStamp)
		:playerPos(Pos), playerRot(Rot), playerTimeStamp(timeStamp)
	{}
	LocalPlayerAction()
	{}

};
enum GameMessages
{
	ID_GAME_MESSAGE_1 = ID_USER_PACKET_ENUM + 1,
	ID_PLAYER_ID_REQUEST = ID_USER_PACKET_ENUM + 2,
	ID_REQUEST_GAME_MESSAGE = ID_USER_PACKET_ENUM + 3,
	ID_ACTIVE_GAME_MESSAGE = ID_USER_PACKET_ENUM + 4,
	ID_GAME_UPDATE_MESSAGE = ID_USER_PACKET_ENUM + 5,
	ID_GAME_ACTION_MESSAGE = ID_USER_PACKET_ENUM +6,
	ID_GAME_ASTEROID_MESSAGE = ID_USER_PACKET_ENUM + 7
};

template <typename T>
double Lerp(T a, T b, double a_time)
{
	return (1 - a_time) * a + a_time *b;
}

ClientState Lerp(ClientState a, ClientState b, double a_time)
{
	ClientState  ls;
	ls.x = (int)Lerp(a.x, b.x, a_time);
	ls.y = (int)Lerp(a.y, b.y, a_time);
	return ls;
}

Game* game;
Timer* timer;
void checkForMessages(RakNet::Packet* packet, RakNet::RakPeerInterface* peer);


bool handleConsoleArguments(int argc, char** argv);
char* clientPort;
int clientPortToInt;

char* serverAddress;
int rakTimer;
int fpsTimer;
int upTimer;
bool externalArgumentsInput;


RakNet::Time timeStamp;
RakNet::Time startTime;

int main(int argc, char** argv)
{
	rakTimer = 0;
	fpsTimer = 0;
	externalArgumentsInput = false;
	if (!handleConsoleArguments(argc, argv))
	{
		std::cin.ignore();
		return 0;
	}

	// Create Timer
	timer = new Timer();

	// Create Game
	game = new Game();

	RakNet::RakString playerName;
	playerName = "Player";

	char* str = new char[512];
	//long playerId = rand() % 5;
	RakNet::RakPeerInterface *peer = RakNet::RakPeerInterface::GetInstance();
	RakNet::RakNetGUID peerReference = peer->GetGuidFromSystemAddress(RakNet::UNASSIGNED_SYSTEM_ADDRESS);
	bool isServer;
	RakNet::Packet *packet = nullptr;


	RakNet::SocketDescriptor sd;
	peer->Startup(1, &sd, 1);
	isServer = false;
	char* portSelect = new char[512];
	int portID;

	if (externalArgumentsInput)
	{
		printf("Welcome to team Pong!\n");
		strncpy(portSelect, clientPort, 512);
		portID = clientPortToInt;

		strncpy(str, serverAddress, 512);
	}
	else
	{
		portSelect = new char[512];
		printf("Welcome to team Pong. Enter the desired port, or tap enter to use the default.\n");
		gets_s(portSelect, 512);

		str = new char[512];
		printf("Enter server IP or hit enter for 127.0.0.1\n");
		gets_s(str, 512);
	}

	if (portSelect[0] == 0)
	{
		printf("port select 0");
		portID = SERVER_PORT;
	}
	else
	{
		printf("port scanning");

		sscanf_s(portSelect, "%d", &portID);

	}

	if (str[0] == 0){
		strcpy_s(str, 512, "127.0.0.1");
	}

	printf("Starting the client.\n");
	peer->Connect(str, portID, 0, 0);
	RakNet::RakString mLastMsg;
	int tempCount = 0;


	

	while (1)
	{
		timer->start();

		if (_kbhit() && !game->isGameStarted())
		{

			// Notice what is not here: something to keep our network running.  It's
			// fine to block on gets or anything we want
			// Because the network engine was painstakingly written using threads.
			RakNet::BitStream bsOut;
			char temp[512];
			gets_s(temp);
			RakNet::RakString tempStr = temp;



			if (tempStr == "help" || tempStr == "Help")
			{
				std::cout << "enter Play/play to start a round " << "\n";
			}
			else if (tempStr == "setName" || tempStr == "SetName")
			{
				char sig[512];
				gets_s(sig);
				playerName = sig;
			}
			else  if (tempStr == "play" || tempStr == "Play")
			{
				std::cout << "Waiting for a lobby" << "\n";
				bsOut.Write((RakNet::MessageID)ID_REQUEST_GAME_MESSAGE);
				peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, peerReference, true);
				//gameActive = true;
			}
			else
			{
				bsOut.Write((RakNet::MessageID)ID_GAME_MESSAGE_1);
				bsOut.Write(playerName);
				bsOut.Write(temp);

				peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, peerReference, true);
			}


		}
		
		checkForMessages(packet, peer);
	

		while (game->isGameStarted())
		{
			timeStamp = RakNet::GetTime();
			timeStamp -= startTime;
			game->update();
			game->getGameObjectManager()->setGameTime(timeStamp);
			if (game->getInputManager()->escapePressed())
			{
				game->shutdown();
			
				
			}
			if (game->getInputManager()->isUpKeyDown())
			{
				game->getGameObjectManager()->getLocalShip()->accelerateShip();
			}
			if (game->getInputManager()->didSpaceKeyChange())
			{
				if (game->getInputManager()->isSpaceKeyDown())
				{


					RakNet::BitStream bsOut;


					LocalPlayerAction tempData (game->getGameObjectManager()->getLocalShip()->getPos(), game->getGameObjectManager()->getLocalShip()->getRotation(), timeStamp);
					//std::cout << game->getGameObjectManager()->getLocalShip()->getmPosX() << " " << game->getGameObjectManager()->getLocalShip()->getmPosY() << " " << game->getGameObjectManager()->getLocalShip()->getRotation() << "\n";
					bsOut.Write((RakNet::MessageID)ID_GAME_ACTION_MESSAGE);
					bsOut.Write(tempData);
					peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, peerReference, true);

					game->getGameObjectManager()->createGameObject(BULLET_IMAGE, game->getGameObjectManager()->getLocalShip()->getPos(), game->getGameObjectManager()->getLocalShip()->getRotation());//getLocalBullet()->updateBulletInformation(game->getGameObjectManager()->getLocalShip()->getPos(), game->getGameObjectManager()->getLocalShip()->getRotation());
				}
					
			}
			if (game->getInputManager()->isLeftKeyDown())
			{
			
				game->getGameObjectManager()->getLocalShip()->rotateShip(rotateDir::_LEFT);
			}
			if (game->getInputManager()->isRightKeyDown())

			{
		
				game->getGameObjectManager()->getLocalShip()->rotateShip(rotateDir::_RIGHT);

			}
			if (timer->getElapsedTime() - rakTimer > 75.0f)
			{
				RakNet::BitStream bsOut;
				
				timeStamp = RakNet::GetTime();
				timeStamp -= startTime;
				//std::cout << timeStamp << "\n";
				LocalPlayerInfo tempLocalInfo(game->getGameObjectManager()->getLocalShip()->getPos(), game->getGameObjectManager()->getLocalShip()->getVelocity(), game->getGameObjectManager()->getLocalShip()->getRotation(), timeStamp);

				rakTimer = timer->getElapsedTime();
				bsOut.Write((RakNet::MessageID)ID_GAME_UPDATE_MESSAGE);
				bsOut.Write(tempLocalInfo);
				bsOut.Write(game->playerHasDied);
				game->playerHasDied = false;
				peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, peerReference, true);
				//std::cout << timer->getElapsedTime()/fpsTimer;
				fpsTimer++;
				RakNet::BitStream bsOut2;

				bsOut2.Write((RakNet::MessageID)ID_GAME_ASTEROID_MESSAGE);
				bsOut2.Write(game->getGameObjectManager()->getAsteroids().size());
				for (int i = 0; i < game->getGameObjectManager()->getAsteroids().size(); i++)
				{
					bsOut2.Write(game->getGameObjectManager()->getAsteroids()[i]);


				}
				
				peer->Send(&bsOut2, HIGH_PRIORITY, RELIABLE_ORDERED, 0, peerReference, true);

			}
		


		
			checkForMessages(packet, peer);
			timer->sleepUntilElapsed(UPDATE_TIME);

		}

		timer->sleepUntilElapsed(UPDATE_TIME);

	}

	game->shutdown();
	game->uninit();
	delete game;

	RakNet::RakPeerInterface::DestroyInstance(peer);

	return 0;
}


void checkForMessages(RakNet::Packet* packet, RakNet::RakPeerInterface* peer)
{
	for (packet = peer->Receive(); packet; peer->DeallocatePacket(packet), packet = peer->Receive())
	{
		switch (packet->data[0])
		{
		case ID_REMOTE_DISCONNECTION_NOTIFICATION:
			printf("Another client has disconnected.\n");
			break;
		case ID_REMOTE_CONNECTION_LOST:
			printf("Another client has lost the connection.\n");
			break;
		case ID_REMOTE_NEW_INCOMING_CONNECTION:
			printf("Another client has connected.\n");
			break;
		case ID_CONNECTION_REQUEST_ACCEPTED:
		{
			printf("Our connection request has been accepted.\nWelcome to the lobby. enter help for tips/keys\n");

			// Use a BitStream to write a custom user message
			// Bitstreams are easier to use than sending casted structures, and handle endian swapping automatical
			break;
		}

		case ID_NEW_INCOMING_CONNECTION:
			printf("A connection is incoming.\n");
			break;
		case ID_NO_FREE_INCOMING_CONNECTIONS:
			printf("The server is full.\n");
			break;
		case ID_GAME_MESSAGE_1:
		{
			RakNet::RakString rs;
			RakNet::RakString ns;
			RakNet::BitStream bsIn(packet->data, packet->length, false);
			bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
			bsIn.Read(ns);
			bsIn.Read(rs);
			std::cout << ns << " : " << rs << "\n";

			/*if (playerId > 0)
			{
			RakNet::BitStream bsOut;
			bsOut.Write((RakNet::MessageID)ID_PLAYER_ID_REQUEST);
			bsOut.Write("ID Reqest");
			std::cout << "ID REQUESTED";
			peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, peerReference, true);
			}*/

			break;
		}
		case ID_ACTIVE_GAME_MESSAGE:
		{
			std::cout << "Game Activated " << "\n";
			RakNet::RakString ns;
			
			RakNet::BitStream bsIn(packet->data, packet->length, false);
			bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
			bsIn.Read(ns);
			startTime = RakNet::GetTime();
			game->init();
		
			game->startup(ns.ToInteger(ns) - 48);
			//std::cout << startTime <<"\n";
			break;
		}
		case ID_GAME_UPDATE_MESSAGE:
		{
			LocalPlayerInfo networkPlayerInfo;
			RakNet::BitStream bsIn(packet->data, packet->length, false);
			bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
			bsIn.Read(networkPlayerInfo);
			



			game->getGameObjectManager()->upgradeNetwork(networkPlayerInfo.playerPos, networkPlayerInfo.playerVel, networkPlayerInfo.playerRot, networkPlayerInfo.playerTimeStamp);

			
			break;
		}
		case ID_GAME_ACTION_MESSAGE :
		{
			LocalPlayerAction networkPlayerInfo;
			bool temp;

			RakNet::BitStream bsIn(packet->data, packet->length, false);
			bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
			bsIn.Read(networkPlayerInfo);
			bsIn.Read(temp);


			if (temp)
			{
				game->decreasePlayerLives();
				//game->playerHasDied = false;
			}
				


			game->getGameObjectManager()->upgradeNetwork(networkPlayerInfo.playerPos, networkPlayerInfo.playerRot, networkPlayerInfo.playerTimeStamp);


			break;
		}
		case ID_GAME_ASTEROID_MESSAGE:
		{
			int astNum;
			int tempNum;
			bool good = false;
			std::vector<int> tempIds;
			RakNet::BitStream bsIn(packet->data, packet->length, false);
			bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
			bsIn.Read(astNum);
			//std::cout << astNum << "\n";
			for (int k = 0; k < astNum; k++)
			{
				bsIn.Read(tempNum);
				std::cout << tempNum << std::endl;
				tempIds.push_back(tempNum);
			}


			for (int i = 0; i < game->getGameObjectManager()->getAsteroids().size(); i++)
			{
				good = false;
				for (int j = 0; j < tempIds.size(); j++)
				{
					if (game->getGameObjectManager()->getAsteroids()[i] == tempIds[j])
					{
						good = true;
						//std::cout << "Index " << i << " was destroyed! \n";
						break;
					}
				}
				if (!good)
				{
					game->getGameObjectManager()->deleteAsteroidWithID(game->getGameObjectManager()->getAsteroids()[i]);
					break;
				}

			}
			//game->getGameObjectManager()->deleteAsteroidWithID(astNum);

			break;
		}

		default:
			printf("Message with identifier %i has arrived.\n", packet->data[0]);
			break;
		}
	}
}

bool handleConsoleArguments(int argc, char** argv)
{
	// -- Reads and sets arguments -- //
	printf("Handling Arguments... \n");

	if (argc == 1)
	{
		printf("No Arguments Found\n\n");

		externalArgumentsInput = false;
		return true;
	}

	if (argc == 3)
	{
		// client
		clientPort = argv[1];
		clientPortToInt = atoi(clientPort);

		serverAddress = argv[2];


		printf("Arguments Read\n\n");

		externalArgumentsInput = true;
		return true;
	}
	else
	{
		printf("I don't know what to do with these arguments");
		std::cerr << "Usage: " << argv[1] << " Client Port" << std::endl;
		std::cerr << "Usage: " << argv[2] << " Server Address" << std::endl;
		std::cerr << "Usage: " << argv[3] << " ServerPort" << std::endl;

		return false;
	}
	
}

