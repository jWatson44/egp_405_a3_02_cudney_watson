#include "Game.h"

Game::Game()
{
	bGameStarted = false;
	mpGraphicsManager = NULL;
	mpGameObjectManager = NULL;
	mpInputManager = NULL;
	mpPhysicsManager = NULL;
}

void Game::init()
{
	bGameStarted = false;

	// Creates and initializes Graphics Manager
	mpGraphicsManager = new GraphicsManager();

	mpInputManager = new InputManager();

	mpPhysicsManager = new PhysicsManager();

	// Creates and initializes GameObject Manager
	mpGameObjectManager = new GameObjectManager(this, mpGraphicsManager, mpPhysicsManager);
}

void Game::uninit()
{
	// Uninit and delete graphics manager
	mpGraphicsManager->uninit();
	delete mpGraphicsManager;

	// Uninit and delete game object manager
	mpGameObjectManager->deleteAllObjects();
	delete mpGameObjectManager;

	delete mpInputManager;


}

void Game::startup(int playerNum)
{
	mPlayerNum = playerNum;
	sf::Vector2f pos;
	mpGameObjectManager->setPlayerNum(playerNum);

	mGameLevel = 0;
	mGameLevelIteration = 0;
	playerLives = 3;
	playerHasDied = false;
	/*

	// Create the player and teammate
	if (playerNum == 1)
	{
	// Spawn Player
	pos = sf::Vector2f(300, 700);
	mpGameObjectManager->createGameObject(MY_PADDLE_ID, pos, 0);

	// Spawn Teammate
	pos = sf::Vector2f(980, 700);
	mpGameObjectManager->createGameObject(TEAMMATE_PADDLE_ID, pos, 0);
	}
	else
	{
	// Spawn Player
	pos = sf::Vector2f(980, 700);
	mpGameObjectManager->createGameObject(MY_PADDLE_ID, pos, 0);

	// Spawn Teammate
	pos = sf::Vector2f(300, 700);
	mpGameObjectManager->createGameObject(TEAMMATE_PADDLE_ID, pos, 0);
	}

	// Create the ball

	// Create brick wall
	mpGameObjectManager->createBrickWall(12, 5);

	// Create the hearts


	*/
	
	// Startup graphics
	mpGraphicsManager->startGraphics(SCREEN_DIMENSIONS);

	// Startup Physics
	mpPhysicsManager->initPhysicsWorld(b2Vec2_zero); //b2Vec2(0, 98));//

	// Create the hearts
	mpGameObjectManager->createLives();
	
	// Spawn Players
	pos = sf::Vector2f(640, 600);
	mpGameObjectManager->createGameObject(MY_SHIP, pos, 0);
	mpGameObjectManager->createGameObject(TEAMMATE_SHIP, pos, 0);
	//mpGameObjectManager->createGameObject(BULLET_IMAGE, pos, 0);

	// Spawn Asteroids
	mpGameObjectManager->spawnAsteroids(mGameLevel, 6, 20);

	bGameStarted = true;
}

void Game::update()
{
	if (!mpGameObjectManager->shouldShutdown())
	{
		mpPhysicsManager->updateWorld();

		mpGraphicsManager->clearWindow();

		mpInputManager->update();

		mpGameObjectManager->update();

		mpGameObjectManager->draw(mpGraphicsManager->getRenderWindow());

		mpGraphicsManager->draw();
	}
	else
		shutdown();

}

void Game::shutdown()
{
	// Remove all game objects
	mpGameObjectManager->deleteAllObjects();

	// Shutdown graphics
	mpGraphicsManager->shutdownGraphics();

	// Shutdown physics
	mpPhysicsManager->uninitPhysicsWorld();

	bGameStarted = false;
}
