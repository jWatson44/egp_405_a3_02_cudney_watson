#include "PhysicsManager.h"
#include "GameObject.h"

PhysicsManager::PhysicsManager()
{
}

PhysicsManager::~PhysicsManager()
{
}

void PhysicsManager::initPhysicsWorld(b2Vec2 gravity)
{
	mGravity = gravity;
	mpWorld = new b2World(mGravity);
	mpContactListener = new ContactListener();
	mpWorld->SetContactListener(mpContactListener);

	// Bodies are excluded from simulation if nothing is happening to them
	mpWorld->SetAllowSleeping(true);
	mpWorld->SetContinuousPhysics(true);
}

void PhysicsManager::uninitPhysicsWorld()
{
	delete mpWorld;
	mpWorld = NULL;
}

void PhysicsManager::updateWorld()
{
	mpWorld->Step(TIME_STEP, VELOCITY_ITERATIONS, POSITION_ITERATIONS);
}

b2Body* PhysicsManager::createPhysicsObject(int xPos, int yPos, float density, float friction, bool isSensor)
{
	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;
	bodyDef.position = b2Vec2(xPos, yPos);
	b2Body* body = mpWorld->CreateBody(&bodyDef);

	b2CircleShape physicsObjectShape;
	physicsObjectShape.m_p.Set(0, 0);
	physicsObjectShape.m_radius = 20;

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &physicsObjectShape;
	fixtureDef.density = density;
	fixtureDef.friction = friction;

	fixtureDef.isSensor = isSensor;

	body->CreateFixture(&fixtureDef);

	return body;
}
