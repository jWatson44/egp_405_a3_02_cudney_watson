#include <stdio.h>
#include <iostream>
#include <string.h>
#include <vector>
#include "RakPeerInterface.h"
#include "MessageIdentifiers.h"
#include "BitStream.h"
#include "RakNetTypes.h"  // MessageID
#include "RakNetTime.h"
#include  "GetTime.h"

#define MAX_CLIENTS 10
#define SERVER_PORT 60000

enum GameMessages
{
	ID_GAME_MESSAGE_1 = ID_USER_PACKET_ENUM + 1,
	ID_PLAYER_ID_REQUEST = ID_USER_PACKET_ENUM + 2,
	ID_REQUEST_GAME_MESSAGE = ID_USER_PACKET_ENUM + 3,
	ID_ACTIVE_GAME_MESSAGE = ID_USER_PACKET_ENUM + 4,
	ID_GAME_UPDATE_MESSAGE = ID_USER_PACKET_ENUM + 5,
	ID_GAME_ACTION_MESSAGE = ID_USER_PACKET_ENUM + 6,
	ID_GAME_ASTEROID_MESSAGE = ID_USER_PACKET_ENUM + 7
};
struct userID
{
	RakNet::SystemAddress systemAdress;
	RakNet::RakNetGUID userGuid;

	long userIdNumber; 
};
struct gamePair
{
	userID playerOne ;
	userID playerTwo ;
	bool isactive = false;
	

};

bool handleConsoleArguments(int argc, char* argv[]);
char *serverPort;
int serverPortInt;

bool externalArgumentsInput;

int main(int argc, char* argv[])
{ 
	externalArgumentsInput = false;
	if (!handleConsoleArguments(argc, argv))
	{
		std::cin.ignore();
		return 0;
	}


	//Game Server Info
	long userCounter; 
	std::vector<userID> userInformation;
	std::vector<gamePair> gameLobby; 
	std::vector<gamePair> activeGameLobby; 
	


	RakNet::RakPeerInterface *peer = RakNet::RakPeerInterface::GetInstance();
	bool isServer;
	RakNet::Packet *packet;
	
	char portSelect[512];
	int portID;

	if (externalArgumentsInput)
	{
		//strncpy_s(portSelect, serverPort, 512);
		strncpy_s(portSelect, serverPort, 512);
	}
	else
	{
		printf("Enter the desired port, or tap enter to use the default.\n");
		gets_s(portSelect, 512);
	}

	if (portSelect[0] == 0)
	{
		portID = SERVER_PORT;
	}
	else
	{
		sscanf_s(portSelect, "%d", &portID);
	}
	
	RakNet::SocketDescriptor sd(portID, 0);
	peer->Startup(MAX_CLIENTS, &sd, 1);
	isServer = true;
	userCounter = 0; 


	printf("Starting the server.\n");
	// We need to let the server accept incoming connections from the clients
	peer->SetMaximumIncomingConnections(MAX_CLIENTS);
	
	while (1)
	{
		for (packet = peer->Receive(); packet; peer->DeallocatePacket(packet), packet = peer->Receive())
		{
			switch (packet->data[0])
			{
			case ID_REMOTE_DISCONNECTION_NOTIFICATION:
				printf("Another client has disconnected.\n");
				break;
			case ID_REMOTE_CONNECTION_LOST:
				printf("Another client has lost the connection.\n");
				break;
			case ID_REMOTE_NEW_INCOMING_CONNECTION:
				printf("Another client has connected.\n");
				break;
			case ID_CONNECTION_REQUEST_ACCEPTED:
			{
				printf("Our connection request has been accepted.\n");

				// Use a BitStream to write a custom user message
						break;
			}
	
		
			case ID_NEW_INCOMING_CONNECTION:
			{
				printf("A connection is incoming.\n");
				userID tempData;

				tempData.systemAdress = packet->systemAddress;
				tempData.userGuid = packet->guid;
				tempData.userIdNumber = userCounter;
				userInformation.push_back(tempData);
				//std::cout << "User with SA of " << userInformation[userInformation.size()-1].systemAdress << " and GUID of " << userInformation[userInformation.size()-1].userGuid;



				userCounter++;
				break;
			}
			
			case ID_NO_FREE_INCOMING_CONNECTIONS:
				printf("The server is full.\n");
				break;
			case ID_DISCONNECTION_NOTIFICATION:
				if (isServer){
					printf("A client has disconnected.\n");
				}
				else {
					printf("We have been disconnected.\n");
				}
				break;
			case ID_CONNECTION_LOST:
				if (isServer){
					printf("A client lost the connection.\n");
				}
				else {
					printf("Connection lost.\n");
				}
				break;

			case ID_GAME_MESSAGE_1:
			{
				RakNet::RakString rs;
				RakNet::BitStream bsIn(packet->data, packet->length, false);
				peer->Send(&bsIn, HIGH_PRIORITY, RELIABLE_ORDERED, 0, packet->systemAddress, true);
				std::cout << "Message Passed" << "\n";
				break;
			}
				
			case ID_PLAYER_ID_REQUEST :
			{
				//RakNet::BitStream bsOut;
				//bsOut.Write((RakNet::MessageID)ID_PLAYER_ID_REQUEST);
				//bsOut.Write(userCounter);
				//std::cout << "userCounter updated " << userCounter << "\n";
				//userCounter++; 
				//peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, packet->systemAddress,true);
			
				break;
			}
			case ID_REQUEST_GAME_MESSAGE : 
			{
				if (gameLobby.size() < 1)
				{
					gamePair newGame;
					newGame.playerOne.systemAdress = packet->systemAddress;
					newGame.playerOne.userGuid = packet->guid;
					gameLobby.push_back(newGame);
					std::cout << "Game lobby started";
					RakNet::BitStream bsIn;
					bsIn.Write((RakNet::MessageID)ID_GAME_MESSAGE_1);
					bsIn.Write("Looking for players");
					peer->Send(&bsIn, HIGH_PRIORITY, RELIABLE_ORDERED, 0, packet->systemAddress, true);
					std::cout << "Message Passed" << "\n";
					
				}
				else
				{
					gameLobby[gameLobby.size() - 1].playerTwo.userGuid = packet->guid;
					gameLobby[gameLobby.size() - 1].playerTwo.systemAdress = packet->systemAddress;
					gameLobby[gameLobby.size() - 1].isactive = true;
					activeGameLobby.push_back(gameLobby[gameLobby.size() - 1]);
					gamePair tempGamData;
					tempGamData.playerOne = activeGameLobby[gameLobby.size() - 1].playerOne;
					tempGamData.playerTwo = activeGameLobby[gameLobby.size() - 1].playerTwo;
					gameLobby.pop_back();
					std::cout << "Game lobby activated";

					RakNet::BitStream bsIn(packet->data, packet->length, false);

					RakNet::Time tempStamp;
					tempStamp = RakNet::GetTime();
					RakNet::BitStream bsOut;
					std::cout << tempStamp;
					bsOut.Write((RakNet::MessageID)ID_ACTIVE_GAME_MESSAGE);
					bsOut.Write(tempStamp);
					bsOut.Write("0");
					peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, activeGameLobby[activeGameLobby.size()-1].playerOne.systemAdress, false);			
					
					RakNet::BitStream bsOut1;
					bsOut1.Write((RakNet::MessageID)ID_ACTIVE_GAME_MESSAGE);
					bsOut1.Write(tempStamp );
					bsOut1.Write("1");
					peer->Send(&bsOut1, HIGH_PRIORITY, RELIABLE_ORDERED, 0, activeGameLobby[activeGameLobby.size() - 1].playerTwo.systemAdress, false);
					//std::cout << tempGamData.playerOne.systemAdress.ToString() << "  " << tempGamData.playerTwo.systemAdress.ToString();					
				}
				break;
			}
			case ID_GAME_UPDATE_MESSAGE:
			{ 

				RakNet::BitStream bsOut(packet->data, packet->length, false);
		
				//bsOut.Write();
				for (unsigned int i = 0; i < activeGameLobby.size(); i++)
				{
					if (activeGameLobby[i].playerOne.userGuid == packet->guid)
					{
						peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, activeGameLobby[i].playerTwo.systemAdress, false);
						i += activeGameLobby.size();

					}
					else if (activeGameLobby[i].playerTwo.userGuid == packet->guid)
					{
						peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, activeGameLobby[i].playerOne.systemAdress, false);
						i += activeGameLobby.size();

					}
				}
				break;
			}
			case ID_GAME_ACTION_MESSAGE:
			{

				RakNet::BitStream bsOut(packet->data, packet->length, false);

				//bsOut.Write();
				for (unsigned int i = 0; i < activeGameLobby.size(); i++)
				{
					if (activeGameLobby[i].playerOne.userGuid == packet->guid)
					{
						peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, activeGameLobby[i].playerTwo.systemAdress, false);
						i += activeGameLobby.size();

					}
					else if (activeGameLobby[i].playerTwo.userGuid == packet->guid)
					{
						peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, activeGameLobby[i].playerOne.systemAdress, false);
						i += activeGameLobby.size();

					}
				}
				break;
			}
			case ID_GAME_ASTEROID_MESSAGE:
			{

				RakNet::BitStream bsOut(packet->data, packet->length, false);

				//bsOut.Write();
				for (unsigned int i = 0; i < activeGameLobby.size(); i++)
				{
					if (activeGameLobby[i].playerOne.userGuid == packet->guid)
					{
						peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, activeGameLobby[i].playerTwo.systemAdress, false);
						i += activeGameLobby.size();

					}
					else if (activeGameLobby[i].playerTwo.userGuid == packet->guid)
					{
						peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, activeGameLobby[i].playerOne.systemAdress, false);
						i += activeGameLobby.size();

					}
				}
				break;
			}

			default:
				printf("Message with identifier %i has arrived.\n", packet->data[0]);
				break;
			}
		}
	}


	RakNet::RakPeerInterface::DestroyInstance(peer);

	return 0;
} 

bool handleConsoleArguments(int argc, char* argv[])
{
	printf("Handling Arguments... \n");

	if (argc == 1) 
	{
		externalArgumentsInput = false;

		printf("No Arguments Found.\n\n");
		return true;
	}
	
	if (argc == 2)
	{
		// server
		serverPort = argv[1];
		serverPortInt = atoi(serverPort);

		externalArgumentsInput = true;

		printf("Arguments Read.\n\n");
		return true;
	}
	else
	{
		printf("I don't know what to do with these arguments");
		std::cerr << "Usage: " << argv[1] << " ServerPort" << std::endl;

		return false;
	}

}

